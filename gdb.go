package gdb

// Control connections to the database
// allows the sharing of the db connection through the whole project

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"
)

// Global Database Objetion
var Connection *Database

// Define the Database type
type Database struct {
	Engine     string
	CS         string
	Connection *sql.DB
}

// Connect to the db
func Connect(engine string, cs string) error {

	// if the connection has alread been made, return it
	if Connection != nil {
		return nil
	}

	// create connection to the db
	connection, err := sql.Open(engine, cs)
	if err != nil {
		return errors.New("Connection Error.")
	}

	// verify connection worked
	err = connection.Ping()
	if err != nil {
		return errors.New("Connection Error.")
	}

	// create the new db object
	Connection = new(Database)

	// set the db settings
	Connection.Engine = engine
	Connection.CS = cs
	Connection.Connection = connection

	return nil
}

// Destroy the db
func (db *Database) Destroy() {
	db.Connection.Close()
}

// Query the db
func (db *Database) Query(query string, args ...interface{}) (*sql.Rows, error) {
	return db.Connection.Query(query, args...)
}
