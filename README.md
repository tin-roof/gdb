# gdb

Global DB handler for go. (currently only handles postgresql)

## Dependencies

Need to get the pq library before we start.

`$ go get -u github.com/lib/pq`

## Getting

To start using gdb run go get:

`$ go get -u gitlab.com/tin-roof/gdb`

This will grab the package for you to use.

## Usage

To start using gdb for your database connection.

``` 
package main

import (
    "gitlab.com/tin-roof/gdb"
    "pkg/queryTest"
)

func main() {
	// create the settings
	settings.Init()

	// connect to the db
	cErr := gdb.Connect("postgres", "user=$$$$ password=$$$$ host=$$$$ port=5432 dbname=$$$$ sslmode=disable")
	if cErr != nil {
		// Handle the connection error
		fmt.Println(cErr.Error())
	}
	defer gdb.Connection.Destroy()

    queryTest.Name();
}
```

Will create a connection to the database that can be used across your project, and packages.

```
package queryTest

import (
    "fmt"
    "gitlab.com/tin-roof/gdb"
)

// Get current software version
func Name() library.JSON {
	// query the db to get the current version of the software
	var query string = `SELECT name FROM table WHERE id = 1;`

	results, err := gdb.Connection.Query(query)
	if err != nil {
		// @TODO connection error
	}
	defer results.Close()

	// sort the data out
	var name string
	for results.Next() {
		if err := results.Scan(&name); err == nil {
			fmt.Println(name);
		}
	}
}
```

The second package makes a query and prints the result, the gdb package uses the connection created in the main package, saving connections to the db.